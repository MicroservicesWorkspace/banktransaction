package com.vfop.transaction.common.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by flooprea on 10/10/2020
 */
public class Event<K, T> implements Serializable {

    private K key;

    private T data;

    private LocalDateTime eventCreatedAt;

    public Event() {
    }

    public Event(K key, T data) {
        this.key = key;
        this.data = data;
        this.eventCreatedAt = LocalDateTime.now();
    }

    public K getKey() {
        return key;
    }

    public T getData() {
        return data;
    }

    public LocalDateTime getEventCreatedAt() {
        return eventCreatedAt;
    }
}
