package com.vfop.transaction.common.dto.core.account;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by flooprea on 10/8/2020
 */
public class AccountDTO implements Serializable {

    private Long id;

    private String iban;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(iban, that.iban);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, iban);
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "id=" + id +
                ", iban='" + iban + '\'' +
                '}';
    }
}
