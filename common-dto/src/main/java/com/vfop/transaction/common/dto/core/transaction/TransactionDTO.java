package com.vfop.transaction.common.dto.core.transaction;

import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import com.vfop.transaction.common.dto.core.customer.CustomerDTO;
import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by flooprea on 10/8/2020
 */
public class TransactionDTO implements Serializable {

    private Long id;

    private CustomerDTO payer;

    private CustomerDTO payee;

    @NotNull
    private BigDecimal amount;

    private TransactionTypeDTO transactionType;

    @NotNull
    private Long transactionTypeId;

    private CurrencyDTO currency;

    @NotNull
    private Long currencyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerDTO getPayer() {
        return payer;
    }

    public void setPayer(CustomerDTO payer) {
        this.payer = payer;
    }

    public CustomerDTO getPayee() {
        return payee;
    }

    public void setPayee(CustomerDTO payee) {
        this.payee = payee;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public TransactionTypeDTO getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionTypeDTO transactionType) {
        this.transactionType = transactionType;
    }

    public Long getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(Long transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public CurrencyDTO getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyDTO currency) {
        this.currency = currency;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionDTO that = (TransactionDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(payer, that.payer) &&
                Objects.equals(payee, that.payee) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(transactionType, that.transactionType) &&
                Objects.equals(transactionTypeId, that.transactionTypeId) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(currencyId, that.currencyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, payer, payee, amount, transactionType, transactionTypeId, currency, currencyId);
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "id=" + id +
                ", payer=" + payer +
                ", payee=" + payee +
                ", amount=" + amount +
                ", transactionType=" + transactionType +
                ", transactionTypeId=" + transactionTypeId +
                ", currency=" + currency +
                ", currencyId=" + currencyId +
                '}';
    }
}
