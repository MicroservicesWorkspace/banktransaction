package com.vfop.transaction.common.dto.report;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by flooprea on 10/11/2020
 */
public class DetailedTransaction {

    private String payerName;

    private String payerCnp;

    private String payerIban;

    private String payeeName;

    private String payeeCnp;

    private String payeeIban;

    private BigDecimal amount;

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getPayerCnp() {
        return payerCnp;
    }

    public void setPayerCnp(String payerCnp) {
        this.payerCnp = payerCnp;
    }

    public String getPayerIban() {
        return payerIban;
    }

    public void setPayerIban(String payerIban) {
        this.payerIban = payerIban;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getPayeeCnp() {
        return payeeCnp;
    }

    public void setPayeeCnp(String payeeCnp) {
        this.payeeCnp = payeeCnp;
    }

    public String getPayeeIban() {
        return payeeIban;
    }

    public void setPayeeIban(String payeeIban) {
        this.payeeIban = payeeIban;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailedTransaction that = (DetailedTransaction) o;
        return Objects.equals(payerName, that.payerName) &&
                Objects.equals(payerCnp, that.payerCnp) &&
                Objects.equals(payerIban, that.payerIban) &&
                Objects.equals(payeeName, that.payeeName) &&
                Objects.equals(payeeCnp, that.payeeCnp) &&
                Objects.equals(payeeIban, that.payeeIban) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(payerName, payerCnp, payerIban, payeeName, payeeCnp, payeeIban, amount);
    }

    @Override
    public String toString() {
        return "TransactionDetails{" +
                "payerName='" + payerName + '\'' +
                ", payerCnp='" + payerCnp + '\'' +
                ", payerIban='" + payerIban + '\'' +
                ", payeeName='" + payeeName + '\'' +
                ", payeeCnp='" + payeeCnp + '\'' +
                ", payeeIban='" + payeeIban + '\'' +
                ", amount=" + amount +
                '}';
    }
}
