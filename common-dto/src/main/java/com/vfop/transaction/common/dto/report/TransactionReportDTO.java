package com.vfop.transaction.common.dto.report;

import java.util.*;

/**
 * Created by flooprea on 10/8/2020
 */
public class TransactionReportDTO {

    private String name;

    private String cnp;

    private String iban;

    private List<TransactionStats> transactionsReport = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public List<TransactionStats> getTransactionsReport() {
        return transactionsReport;
    }

    public void setTransactionsReport(List<TransactionStats> transactionsReport) {
        this.transactionsReport = transactionsReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionReportDTO that = (TransactionReportDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(cnp, that.cnp) &&
                Objects.equals(iban, that.iban) &&
                Objects.equals(transactionsReport, that.transactionsReport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cnp, iban, transactionsReport);
    }

    @Override
    public String toString() {
        return "TransactionReportDTO{" +
                "name='" + name + '\'' +
                ", cnp='" + cnp + '\'' +
                ", iban='" + iban + '\'' +
                ", transactionsReport=" + transactionsReport +
                '}';
    }
}
