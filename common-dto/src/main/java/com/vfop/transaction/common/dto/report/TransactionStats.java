package com.vfop.transaction.common.dto.report;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by flooprea on 10/11/2020
 */
public class TransactionStats {

    private BigInteger id;

    private String name;

    private BigInteger numberOfTransactions;

    private BigDecimal totalAmount;

    private List<DetailedTransaction> transactions = new ArrayList<>();

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getNumberOfTransactions() {
        return numberOfTransactions;
    }

    public void setNumberOfTransactions(BigInteger numberOfTransactions) {
        this.numberOfTransactions = numberOfTransactions;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<DetailedTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<DetailedTransaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionStats that = (TransactionStats) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(numberOfTransactions, that.numberOfTransactions) &&
                Objects.equals(totalAmount, that.totalAmount) &&
                Objects.equals(transactions, that.transactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, numberOfTransactions, totalAmount, transactions);
    }

    @Override
    public String toString() {
        return "TransactionStats{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", numberOfTransactions=" + numberOfTransactions +
                ", totalAmount=" + totalAmount +
                ", transactions=" + transactions +
                '}';
    }
}
