package com.vfop.transaction.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Created by flooprea on 10/12/2020
 */
public class CustomBadRequestException extends HttpClientErrorException {

    public CustomBadRequestException(HttpStatus statusCode, String message) {
        super(statusCode, message);
    }
}
