package com.vfop.transaction.api.exception;

/**
 * Created by flooprea on 10/9/2020
 */
public class ExceptionDetails extends Exception {

    private long timestamp;

    private String message;

    public ExceptionDetails(long timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
