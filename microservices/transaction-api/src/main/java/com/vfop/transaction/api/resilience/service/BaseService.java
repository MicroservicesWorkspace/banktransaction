package com.vfop.transaction.api.resilience.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Created by flooprea on 10/12/2020
 */
@Service
public class BaseService {

    private final WebClient.Builder webClientBuilder;

    private WebClient webClient;

    @Autowired
    public BaseService(WebClient.Builder webClientBuilder) {
        this.webClientBuilder = webClientBuilder;
    }

    public WebClient getWebClient() {
        if (webClient == null) {
            webClient = webClientBuilder.build();
        }

        return webClient;
    }
}
