package com.vfop.transaction.api.resilience.service;

import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/12/2020
 */

@Service
public class CurrencyResilientService extends BaseService {

    private static final Logger logger = LoggerFactory.getLogger(CurrencyResilientService.class);

    private static final String transactionPersistenceUrl = "http://transaction-persistence/";

    @Autowired
    public CurrencyResilientService(WebClient.Builder webClientBuilder) {
        super(webClientBuilder);
    }

    @Retry(name = "currency", fallbackMethod = "retryCurrencyFallback")
    public Flux<CurrencyDTO> getAllCurrencies() {
        logger.info("Getting the currencies...");

        return getWebClient()
                .get()
                .uri(transactionPersistenceUrl + "/currency")
                .retrieve()
                .bodyToFlux(CurrencyDTO.class).log();
    }

    public Flux<String> retryCurrencyFallback(Throwable t) {
        logger.error("Inside retryCurrencyFallback, cause - {}", t.getMessage());

        return Flux.just("Inside retryCurrencyFallback method. Some error occurred while calling service getAllCurrencies!");
    }
}
