package com.vfop.transaction.api.resilience.service;

import com.vfop.transaction.common.dto.report.TransactionReportDTO;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;

/**
 * Created by flooprea on 10/12/2020
 */

@Service
public class TransactionReportResilientService extends BaseService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionReportResilientService.class);

    private static final String transactionPersistenceUrl = "http://transaction-persistence/";

    @Autowired
    public TransactionReportResilientService(WebClient.Builder webClientBuilder) {
        super(webClientBuilder);
    }

    @Retry(name = "report", fallbackMethod = "retryReportFallback")
    public Mono<TransactionReportDTO> getTransactionReportByCnp(String cnp) {
        logger.info("Getting the report of transactions for customer with cnp: {} ...", cnp);

        URI url = UriComponentsBuilder.fromUriString(transactionPersistenceUrl + "/report/{cnp}").build(cnp);

        return getWebClient()
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(TransactionReportDTO.class);
    }

    public Mono<String> retryReportFallback(String cnp, Throwable t) {
        logger.error("Inside retryReportFallback, cause - {}", t.getMessage());

        return Mono.just("Inside retryReportFallback method. Some error occurred while calling service getTransactionReportByCnp!");
    }
}
