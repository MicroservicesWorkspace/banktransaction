package com.vfop.transaction.api.resilience.service;

import com.vfop.transaction.api.exception.CustomBadRequestException;
import com.vfop.transaction.api.exception.ExceptionDetails;
import com.vfop.transaction.api.exception.ValidationException;
import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Date;

/**
 * Created by flooprea on 10/12/2020
 */

@Service
public class TransactionResilientService extends BaseService {

    private static final Logger logger = LoggerFactory.getLogger(CurrencyResilientService.class);

    private static final String transactionValidationUrl = "http://transaction-validation/";

    @Autowired
    public TransactionResilientService(WebClient.Builder webClientBuilder) {
        super(webClientBuilder);
    }


    @Retry(name = "validation")
    public Mono<Boolean> validateTransaction(TransactionDTO transactionDTO) {
        logger.info("Validating transaction: {} ...", transactionDTO);

        return getWebClient()
                .post()
                .uri(transactionValidationUrl + "/validate-transaction")
                .body(Mono.just(transactionDTO), TransactionDTO.class)
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::handleBadRequestExceptions)
                .onStatus(HttpStatus::is5xxServerError, this::handleInternalErrorExceptions).bodyToMono(Boolean.class);
    }

    private Mono<Throwable> handleInternalErrorExceptions(org.springframework.web.reactive.function.client.ClientResponse clientResponse) {
        Mono<String> exceptionMessage = clientResponse.bodyToMono(String.class);
        return exceptionMessage.flatMap(ex -> Mono.error(new ExceptionDetails(new Date().getTime(), ex)));
    }

    private Mono<Throwable> handleBadRequestExceptions(org.springframework.web.reactive.function.client.ClientResponse clientResponse) {
        Mono<ValidationException> exception = clientResponse.bodyToMono(ValidationException.class);
        return exception.flatMap(ex -> Mono.error(new CustomBadRequestException(HttpStatus.BAD_REQUEST, ex.getMessage())));
    }
}
