package com.vfop.transaction.api.resilience.service;

import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import io.github.resilience4j.retry.annotation.Retry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/12/2020
 */

@Service
public class TransactionTypeResilientService extends BaseService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionTypeResilientService.class);

    private static final String transactionPersistenceUrl = "http://transaction-persistence/";

    @Autowired
    public TransactionTypeResilientService(WebClient.Builder webClientBuilder) {
        super(webClientBuilder);
    }

    @Retry(name = "transactionType", fallbackMethod = "retryTransactionTypeFallback")
    public Flux<TransactionTypeDTO> getAllTransactionTypes() {
        logger.info("Getting the currencies...");

        return getWebClient()
                .get()
                .uri(transactionPersistenceUrl + "/transaction-type")
                .retrieve()
                .bodyToFlux(TransactionTypeDTO.class).log();
    }

    public Flux<String> retryTransactionTypeFallback(Throwable t) {
        logger.error("Inside retryTransactionTypeFallback, cause - {}", t.getMessage());

        return Flux.just("Inside retryTransactionTypeFallback method. Some error occurred while calling service getAllTransactionTypes!");
    }
}
