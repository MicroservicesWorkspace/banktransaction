package com.vfop.transaction.api.resource;

import com.vfop.transaction.api.resilience.service.CurrencyResilientService;
import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/12/2020
 */

@RestController
@RequestMapping("/api")
public class CurrencyResource {

    private static final Logger logger = LoggerFactory.getLogger(CurrencyResource.class);

    private final CurrencyResilientService currencyResilientService;

    @Autowired
    public CurrencyResource(CurrencyResilientService currencyResilientService) {
        this.currencyResilientService = currencyResilientService;
    }

    @GetMapping("/currencies")
    Flux<CurrencyDTO> getAllCurrencies() {
        logger.info("REST request to get all currencies...");

        return currencyResilientService.getAllCurrencies();
    }

}
