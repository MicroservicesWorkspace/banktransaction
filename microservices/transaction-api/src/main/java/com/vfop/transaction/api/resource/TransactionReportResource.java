package com.vfop.transaction.api.resource;

import com.vfop.transaction.api.resilience.service.TransactionReportResilientService;
import com.vfop.transaction.common.dto.report.TransactionReportDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created by flooprea on 10/12/2020
 */

@RestController
@RequestMapping("/api")
public class TransactionReportResource {

    private static final Logger logger = LoggerFactory.getLogger(TransactionReportResource.class);

    private final TransactionReportResilientService transactionReportResilientService;

    @Autowired
    public TransactionReportResource(TransactionReportResilientService transactionReportResilientService) {
        this.transactionReportResilientService = transactionReportResilientService;
    }

    @GetMapping("/reports/{cnp}")
    Mono<TransactionReportDTO> getTransactionReportByCnp(@PathVariable(name = "cnp") String cnp) {
        logger.info("REST request to get the report of transactions for customer with cnp: {}...", cnp);

        return transactionReportResilientService.getTransactionReportByCnp(cnp);
    }
}
