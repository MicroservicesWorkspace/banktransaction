package com.vfop.transaction.api.resource;

import com.vfop.transaction.api.resilience.service.TransactionResilientService;
import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/**
 * Created by flooprea on 10/12/2020
 */

@RestController
@RequestMapping("/api")
public class TransactionResource {

    private static final Logger logger = LoggerFactory.getLogger(TransactionResource.class);

    private final TransactionResilientService transactionResilientService;

    @Autowired
    public TransactionResource(TransactionResilientService transactionResilientService) {
        this.transactionResilientService = transactionResilientService;
    }

    @PostMapping("/transactions")
    Mono<Boolean> validateTransaction(@RequestBody @Valid TransactionDTO transactionDTO) {
        logger.info("REST request to validate the transaction {}...", transactionDTO);

        return transactionResilientService.validateTransaction(transactionDTO);
    }
}
