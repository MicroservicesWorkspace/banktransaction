package com.vfop.transaction.api.resource;

import com.vfop.transaction.api.resilience.service.TransactionTypeResilientService;
import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/12/2020
 */

@RestController
@RequestMapping("/api")
public class TransactionTypeResource {

    private static final Logger logger = LoggerFactory.getLogger(TransactionTypeResource.class);

    private final TransactionTypeResilientService transactionTypeResilientService;

    @Autowired
    public TransactionTypeResource(TransactionTypeResilientService transactionTypeResilientService) {
        this.transactionTypeResilientService = transactionTypeResilientService;
    }

    @GetMapping("/transaction-types")
    Flux<TransactionTypeDTO> getAllTransactionTypes() {
        logger.info("REST request to get all transactionTypes...");

        return transactionTypeResilientService.getAllTransactionTypes();
    }
}
