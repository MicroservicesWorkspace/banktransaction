package com.vfop.transaction.api;

import com.vfop.transaction.api.exception.ValidationException;
import com.vfop.transaction.api.resilience.service.CurrencyResilientService;
import com.vfop.transaction.api.resilience.service.TransactionReportResilientService;
import com.vfop.transaction.api.resilience.service.TransactionResilientService;
import com.vfop.transaction.api.resilience.service.TransactionTypeResilientService;
import com.vfop.transaction.api.resource.CurrencyResource;
import com.vfop.transaction.api.resource.TransactionReportResource;
import com.vfop.transaction.api.resource.TransactionResource;
import com.vfop.transaction.api.resource.TransactionTypeResource;
import com.vfop.transaction.common.dto.core.account.AccountDTO;
import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import com.vfop.transaction.common.dto.core.customer.CustomerDTO;
import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import com.vfop.transaction.common.dto.report.DetailedTransaction;
import com.vfop.transaction.common.dto.report.TransactionReportDTO;
import com.vfop.transaction.common.dto.report.TransactionStats;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = {CurrencyResource.class,
        TransactionTypeResource.class,
        TransactionResource.class,
		TransactionReportResource.class})
class TransactionApiApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private CurrencyResilientService currencyResilientService;

    @MockBean
    private TransactionTypeResilientService transactionTypeResilientService;

    @MockBean
    private TransactionResilientService transactionResilientService;

    @MockBean
    private TransactionReportResilientService transactionReportResilientService;

    @Test
    public void givenCurrencies_whenGetCurrencies_thenReturnJsonArray() {
        CurrencyDTO euro = createCurrencyDTO(1L, "EURO");
        CurrencyDTO ron = createCurrencyDTO(2L, "RON");
        CurrencyDTO usd = createCurrencyDTO(3L, "USD");
        CurrencyDTO chf = createCurrencyDTO(4L, "CHF");

        Flux<CurrencyDTO> allCurrencies = Flux.just(euro, ron, usd);
        given(currencyResilientService.getAllCurrencies()).willReturn(allCurrencies);

        webTestClient
                .get()
                .uri("/api/currencies")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(CurrencyDTO.class)
                .contains(euro, ron, usd)
                .hasSize(3)
                .doesNotContain(chf);
    }

    @Test
    public void givenTransactionTypes_wheGetTransactionTypes_thenReturnJsonArray() {
        TransactionTypeDTO ibanToIban = createTransactionTypeDTO(1L, "IBAN_TO_IBAN");
        TransactionTypeDTO ibanToWallet = createTransactionTypeDTO(2L, "IBAN_TO_WALLET");
        TransactionTypeDTO walletToIban = createTransactionTypeDTO(3L, "WALLET_TO_IBAN");
        TransactionTypeDTO walletToWallet = createTransactionTypeDTO(4L, "WALLET_TO_WALLET");

        Flux<TransactionTypeDTO> allTransactionTypes = Flux.just(ibanToIban, ibanToWallet, walletToIban);
        given(transactionTypeResilientService.getAllTransactionTypes()).willReturn(allTransactionTypes);

        webTestClient
                .get()
                .uri("/api/transaction-types")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(TransactionTypeDTO.class)
                .contains(ibanToIban, ibanToWallet, walletToIban)
                .hasSize(3)
                .doesNotContain(walletToWallet);

    }

    @Test
    public void givenTransaction_whenValidateTransaction_thenReturnTrue() {
        AccountDTO account1 = createAccountDTO("RO22INGB0000212337956621");
        AccountDTO account2 = createAccountDTO("RO32BCRB0000124367756356");

        CustomerDTO customer1 = createCustomerDTO("Ion", "Popescu", "1901110469825", account1);
        CustomerDTO customer2 = createCustomerDTO("Ionut", "Vasilache", "1920327386771", account2);

        CurrencyDTO currencyDTO = createCurrencyDTO(1L, "RON");
        TransactionTypeDTO transactionTypeDTO = createTransactionTypeDTO(1L, "IBAN_TO_IBAN");

        TransactionDTO transactionDTO = createTransactionDTO(customer1, customer2, currencyDTO, transactionTypeDTO, BigDecimal.valueOf(250));

        given(transactionResilientService.validateTransaction(transactionDTO)).willReturn(Mono.just(true));

        webTestClient
                .post()
                .uri("/api/transactions")
                .body(BodyInserters.fromValue(transactionDTO))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Boolean.class)
                .isEqualTo(true);
    }

    @Test
    public void givenTransaction_whenValidateTransaction_thenThrowException() {
        AccountDTO account1 = createAccountDTO("RO22INGB0000212337956621");
        AccountDTO account2 = createAccountDTO("RO32BCRB0000124367756356");

        CustomerDTO customer1 = createCustomerDTO("Ion", "Popescu", "1901110469825", account1);
        CustomerDTO customer2 = createCustomerDTO("Ionut", "Vasilache", "1920327386771", account2);

        CurrencyDTO currencyDTO = createCurrencyDTO(1L, "RON");
        TransactionTypeDTO transactionTypeDTO = createTransactionTypeDTO(1L, "IBAN_TO_IBAN");

        TransactionDTO transactionDTO = createTransactionDTO(customer1, customer2, currencyDTO, transactionTypeDTO, null);

        given(transactionResilientService.validateTransaction(transactionDTO)).willReturn(Mono.just(true));

        webTestClient
                .post()
                .uri("/api/transactions")
                .body(BodyInserters.fromValue(transactionDTO))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .is4xxClientError()
                .expectBody(ValidationException.class);
    }

    @Test
    public void getCnp_getTransactionReportByCnp_thenReturnJsonArray() {

    	DetailedTransaction detailedTransaction1 = createDetailedTransaction("Adrian Popa",
				"1901110469825", "RO22INGB0000212337956621",
				"Ionel Vasile", "1920327386771", "RO32BCRB0000124367756356", BigDecimal.valueOf(2500));

		DetailedTransaction detailedTransaction2 = createDetailedTransaction("Adrian Popa",
				"1901110469825", "RO22INGB0333212339823621",
				"Popa Ion", "1900427245925", "RO32BCRB0000124367756356", BigDecimal.valueOf(3000));

		List<DetailedTransaction> detailedTransactions = Arrays.asList(detailedTransaction1, detailedTransaction2);
        TransactionStats transactionStats = createTransactionStats(BigInteger.valueOf(1), "IBAN_TO_IBAN", BigInteger.valueOf(2), BigDecimal.valueOf(5500), detailedTransactions);
        TransactionReportDTO transactionReportDTO = createTransactionReportDTO("Adrian Popa", "1901110469825", "RO32BCRB0000124367756356", Arrays.asList(transactionStats));

        given(transactionReportResilientService.getTransactionReportByCnp("1901110469825")).willReturn(Mono.just(transactionReportDTO));

        webTestClient
				.get()
				.uri("/api/reports/{cnp}", "1901110469825")
				.accept(MediaType.APPLICATION_JSON)
				.exchange()
				.expectStatus()
				.isOk()
				.expectBody(TransactionReportDTO.class)
				.isEqualTo(transactionReportDTO);
    }


    private DetailedTransaction createDetailedTransaction(String payerName,
                                                          String payerCnp,
                                                          String payerIban,
                                                          String payeeName,
                                                          String payeeCnp,
                                                          String payeeIban,
                                                          BigDecimal amount) {
        DetailedTransaction detailedTransaction = new DetailedTransaction();
        detailedTransaction.setPayerName(payerName);
        detailedTransaction.setPayerCnp(payerCnp);
        detailedTransaction.setPayeeIban(payerIban);
        detailedTransaction.setPayeeName(payeeName);
        detailedTransaction.setPayeeIban(payeeIban);
        detailedTransaction.setPayeeCnp(payeeCnp);
        detailedTransaction.setAmount(amount);

        return detailedTransaction;
    }

    private TransactionStats createTransactionStats(BigInteger id,
                                                    String name,
                                                    BigInteger numberOfTransactions,
                                                    BigDecimal amount,
                                                    List<DetailedTransaction> detailedTransactions) {
        TransactionStats transactionStats = new TransactionStats();
        transactionStats.setId(id);
        transactionStats.setName(name);
        transactionStats.setNumberOfTransactions(numberOfTransactions);
        transactionStats.setTotalAmount(amount);
        transactionStats.setTransactions(detailedTransactions);

        return transactionStats;
    }

    private TransactionReportDTO createTransactionReportDTO(String name, String cnp, String iban, List<TransactionStats> transactionStats) {
        TransactionReportDTO transactionReportDTO = new TransactionReportDTO();
        transactionReportDTO.setName(name);
        transactionReportDTO.setCnp(cnp);
        transactionReportDTO.setIban(iban);
        transactionReportDTO.setTransactionsReport(transactionStats);

        return transactionReportDTO;
    }

    private CurrencyDTO createCurrencyDTO(Long id, String name) {
        CurrencyDTO currencyDTO = new CurrencyDTO();
        currencyDTO.setId(id);
        currencyDTO.setName(name);

        return currencyDTO;
    }

    private TransactionTypeDTO createTransactionTypeDTO(Long id, String name) {
        TransactionTypeDTO transactionTypeDTO = new TransactionTypeDTO();
        transactionTypeDTO.setId(id);
        transactionTypeDTO.setName(name);

        return transactionTypeDTO;
    }

    private CustomerDTO createCustomerDTO(String firstName, String lastName, String cnp, AccountDTO accountDTO) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(firstName);
        customerDTO.setLastName(lastName);
        customerDTO.setCnp(cnp);
        customerDTO.setAccount(accountDTO);

        return customerDTO;
    }

    private AccountDTO createAccountDTO(String iban) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setIban(iban);

        return accountDTO;
    }

    private TransactionDTO createTransactionDTO(CustomerDTO payer, CustomerDTO payee,
                                                CurrencyDTO currencyDTO,
                                                TransactionTypeDTO transactionTypeDTO, BigDecimal amount) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setPayer(payer);
        transactionDTO.setPayee(payee);
        transactionDTO.setCurrencyId(currencyDTO.getId());
        transactionDTO.setTransactionTypeId(transactionTypeDTO.getId());
        transactionDTO.setAmount(amount);

        return transactionDTO;
    }
}
