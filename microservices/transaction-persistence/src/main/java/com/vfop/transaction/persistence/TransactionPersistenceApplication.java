package com.vfop.transaction.persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class TransactionPersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransactionPersistenceApplication.class, args);
	}

}
