package com.vfop.transaction.persistence.messaging;

import com.vfop.transaction.common.dto.Event;
import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.persistence.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * Created by flooprea on 10/8/2020
 */

@EnableBinding(Sink.class)
public class MessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(MessageProcessor.class);
    private final TransactionService transactionService;

    @Autowired
    public MessageProcessor(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @StreamListener(target = Sink.INPUT)
    public void process(Event<String, TransactionDTO> event) {
        logger.info("Process message created at {}...", event.getEventCreatedAt());

        transactionService.createTransaction(event.getData());

        logger.info("Message processing done!");
    }
}
