package com.vfop.transaction.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by flooprea on 10/8/2020
 */

@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_id_seq")
    @SequenceGenerator(name = "customer_id_seq", sequenceName = "customer_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "cnp", nullable = false, unique = true)
    private String cnp;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    private Account account;

    @OneToMany(mappedBy = "payer", cascade = CascadeType.ALL)
    private Set<Transaction> payerTransactions = new HashSet<>();

    @OneToMany(mappedBy = "payee", cascade = CascadeType.ALL)
    private Set<Transaction>  payeeTransactions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Set<Transaction> getPayerTransactions() {
        return payerTransactions;
    }

    public void setPayerTransactions(Set<Transaction> payerTransactions) {
        this.payerTransactions = payerTransactions;
    }

    public Set<Transaction> getPayeeTransactions() {
        return payeeTransactions;
    }

    public void setPayeeTransactions(Set<Transaction> payeeTransactions) {
        this.payeeTransactions = payeeTransactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) &&
                Objects.equals(firstName, customer.firstName) &&
                Objects.equals(lastName, customer.lastName) &&
                Objects.equals(cnp, customer.cnp) &&
                Objects.equals(account, customer.account) &&
                Objects.equals(payerTransactions, customer.payerTransactions) &&
                Objects.equals(payeeTransactions, customer.payeeTransactions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, cnp, account, payerTransactions, payeeTransactions);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cnp='" + cnp + '\'' +
                ", account=" + account +
                ", payerTransactions=" + payerTransactions +
                ", payeeTransactions=" + payeeTransactions +
                '}';
    }
}
