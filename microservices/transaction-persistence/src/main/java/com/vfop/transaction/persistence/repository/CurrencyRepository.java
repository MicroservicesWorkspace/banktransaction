package com.vfop.transaction.persistence.repository;

import com.vfop.transaction.persistence.model.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by flooprea on 10/8/2020
 */

@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {
}
