package com.vfop.transaction.persistence.repository;

import com.vfop.transaction.persistence.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by flooprea on 10/8/2020
 */

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findByCnp(String cnp);

    @Query(value = "SELECT DISTINCT C.id, C.first_name, C.last_name, C.cnp, A.iban " +
                   "FROM customer C " +
                   "INNER JOIN account A " +
                   "ON (C.id = A.id) " +
                   "WHERE C.cnp = :cnp", nativeQuery = true)
    Optional<Object> findDetailedCustomerByCnp(@Param("cnp") String cnp);
}
