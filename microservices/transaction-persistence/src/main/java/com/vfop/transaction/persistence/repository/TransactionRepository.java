package com.vfop.transaction.persistence.repository;

import com.vfop.transaction.persistence.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by flooprea on 10/8/2020
 */

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT TT.id, TT.name, COUNT(*) as nb_transactions, SUM(T.amount) " +
            "FROM transaction T " +
            "INNER JOIN transaction_type TT ON T.transaction_type_id = TT.id " +
            "INNER JOIN customer C ON T.payer_id = C.id " +
            "WHERE C.cnp = :cnp " +
            "GROUP BY T.transaction_type_id, TT.name, TT.id " +
            "ORDER BY T.transaction_type_id",
            nativeQuery = true)
    List<Object[]> getReportTransactionsByCnp(@Param("cnp") String cnp);

    @Query(value = "SELECT PR.first_name as payerFirstName, PR.last_name as payerLastName, PR.cnp as payerCnp, PRA.iban as payerIban, " +
            "PE.first_name as payeeFirstName, PE.last_name as payeeLastName, PE.cnp as payeeCnp, PEA.iban as payeeIban, T.amount " +
            "FROM transaction T " +
            "INNER JOIN transaction_type TT ON T.transaction_type_id = TT.id " +
            "INNER JOIN customer PR ON T.payer_id = PR.id " +
            "INNER JOIN account PRA ON PR.account_id = PRA.id " +
            "INNER JOIN customer PE ON T.payee_id = PE.id " +
            "INNER JOIN account PEA ON PE.account_id = PEA.id " +
            "WHERE PR.cnp = :cnp AND TT.id = :transactionTypeId",
            nativeQuery = true)
    List<Object[]> getAllTransactionsByCnpAndTypeId(@Param("cnp") String cnp, @Param("transactionTypeId") BigInteger transactionTypeId);
}
