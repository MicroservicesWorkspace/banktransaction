package com.vfop.transaction.persistence.resource;

import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import com.vfop.transaction.persistence.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/8/2020
 */

@RestController
public class CurrencyResource {

    private final CurrencyService currencyService;

    @Autowired
    public CurrencyResource(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping("/currency")
    public Flux<CurrencyDTO> getAllCurrencies() {
        return currencyService.getAllCurrencies();
    }

}
