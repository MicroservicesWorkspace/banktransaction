package com.vfop.transaction.persistence.resource;

import com.vfop.transaction.common.dto.report.TransactionReportDTO;
import com.vfop.transaction.persistence.service.TransactionReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Created by flooprea on 10/8/2020
 */

@RestController
public class TransactionReportResource {

    private final TransactionReportService transactionReportService;

    @Autowired
    public TransactionReportResource(TransactionReportService transactionReportService) {
        this.transactionReportService = transactionReportService;
    }

    @GetMapping("/report/{cnp}")
    public Mono<TransactionReportDTO> getTransactionReportByCnp(@PathVariable(name = "cnp") String cnp) {
        return transactionReportService.getReportByCnp(cnp);
    }
}
