package com.vfop.transaction.persistence.resource;

import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import com.vfop.transaction.persistence.service.TransactionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/8/2020
 */

@RestController
public class TransactionTypeResource {

    private final TransactionTypeService transactionTypeService;

    @Autowired
    public TransactionTypeResource(TransactionTypeService transactionTypeService) {
        this.transactionTypeService = transactionTypeService;
    }

    @GetMapping("/transaction-type")
    public Flux<TransactionTypeDTO> getAllTransactionTypes() {
        return transactionTypeService.getAllTransactionTypes();
    }
}
