package com.vfop.transaction.persistence.service;

import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

/**
 * Created by flooprea on 10/8/2020
 */

@Service
public interface CurrencyService {

    Flux<CurrencyDTO> getAllCurrencies();
}
