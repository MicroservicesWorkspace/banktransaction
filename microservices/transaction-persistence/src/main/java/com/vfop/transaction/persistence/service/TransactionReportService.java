package com.vfop.transaction.persistence.service;

import com.vfop.transaction.common.dto.report.TransactionReportDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Created by flooprea on 10/10/2020
 */
@Service
public interface TransactionReportService {

    Mono<TransactionReportDTO> getReportByCnp(String cnp);
}
