package com.vfop.transaction.persistence.service;

import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Created by flooprea on 10/8/2020
 */

@Service
public interface TransactionService {

    Mono<TransactionDTO> createTransaction(TransactionDTO transactionDTO);
}
