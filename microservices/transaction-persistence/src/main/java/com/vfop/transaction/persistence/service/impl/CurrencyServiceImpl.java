package com.vfop.transaction.persistence.service.impl;

import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import com.vfop.transaction.persistence.model.Currency;
import com.vfop.transaction.persistence.repository.CurrencyRepository;
import com.vfop.transaction.persistence.service.CurrencyService;
import com.vfop.transaction.persistence.service.mapper.CurrencyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * Created by flooprea on 10/8/2020
 */

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    private final CurrencyMapper currencyMapper;

    @Autowired
    public CurrencyServiceImpl(CurrencyRepository currencyRepository, CurrencyMapper currencyMapper) {
        this.currencyRepository = currencyRepository;
        this.currencyMapper = currencyMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public Flux<CurrencyDTO> getAllCurrencies() {
        List<Currency> currencies = currencyRepository.findAll();

        return Flux.fromIterable(currencies).map(currencyMapper::currencyToCurrencyDTO);
    }
}
