package com.vfop.transaction.persistence.service.impl;

import com.vfop.transaction.common.dto.report.DetailedTransaction;
import com.vfop.transaction.common.dto.report.TransactionReportDTO;
import com.vfop.transaction.common.dto.report.TransactionStats;
import com.vfop.transaction.persistence.repository.CustomerRepository;
import com.vfop.transaction.persistence.repository.TransactionRepository;
import com.vfop.transaction.persistence.service.TransactionReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 10/10/2020
 */
@Service
public class TransactionReportServiceImpl implements TransactionReportService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionReportServiceImpl.class);

    private final TransactionRepository transactionRepository;

    private final CustomerRepository customerRepository;

    @Autowired
    public TransactionReportServiceImpl(TransactionRepository transactionRepository, CustomerRepository customerRepository) {
        this.transactionRepository = transactionRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public Mono<TransactionReportDTO> getReportByCnp(String cnp) {
        List<Object[]> customerReportStats = transactionRepository.getReportTransactionsByCnp(cnp);
        Optional<Object> detailedCustomer = customerRepository.findDetailedCustomerByCnp(cnp);

        if (detailedCustomer.isPresent()) {
            TransactionReportDTO transactionReportDTO = new TransactionReportDTO();
            setCustomerDetails(detailedCustomer.get(), transactionReportDTO);
            setReportStatsAndTransactions(customerReportStats, transactionReportDTO, cnp);
            return Mono.justOrEmpty(transactionReportDTO);
        }

        return Mono.empty();
    }

    private void setReportStatsAndTransactions(List<Object[]> customerReportStats, TransactionReportDTO transactionReportDTO, String cnp) {
        List<TransactionStats> transactionsReport = new ArrayList<>();
        customerReportStats.forEach(report -> {
            TransactionStats transactionStats = new TransactionStats();
            transactionStats.setId((BigInteger) report[0]);
            transactionStats.setName((String) report[1]);
            transactionStats.setNumberOfTransactions((BigInteger) report[2]);
            transactionStats.setTotalAmount((BigDecimal) report[3]);

            List<Object[]> transactions = transactionRepository.getAllTransactionsByCnpAndTypeId(cnp, (BigInteger) report[0]);
            List<DetailedTransaction> detailedTransactions =  transactions
                    .stream()
                    .map(this::mapToDetailedTransaction)
                    .collect(Collectors.toList());

            transactionStats.setTransactions(detailedTransactions);
            transactionsReport.add(transactionStats);
        });
        transactionReportDTO.setTransactionsReport(transactionsReport);
    }

    private DetailedTransaction mapToDetailedTransaction(Object[] detailedTrans) {
        DetailedTransaction detailedTransaction = new DetailedTransaction();
        String payerName = detailedTrans[0] + " " + detailedTrans[1];
        detailedTransaction.setPayerName(payerName);
        detailedTransaction.setPayerCnp((String) detailedTrans[2]);
        detailedTransaction.setPayerIban((String) detailedTrans[3]);
        String payeeName = detailedTrans[4] + " " + detailedTrans[5];
        detailedTransaction.setPayeeName(payeeName);
        detailedTransaction.setPayeeCnp((String) detailedTrans[6]);
        detailedTransaction.setPayeeIban((String) detailedTrans[7]);
        detailedTransaction.setAmount((BigDecimal) detailedTrans[8]);

        return detailedTransaction;
    }

    private void setCustomerDetails(Object detailedCustomer, TransactionReportDTO transactionReportDTO) {
        String name = ((Object[]) detailedCustomer)[1] + " " + ((Object[]) detailedCustomer)[2];
        transactionReportDTO.setName(name);
        transactionReportDTO.setCnp((String) ((Object[]) detailedCustomer)[3]);
        transactionReportDTO.setIban((String) ((Object[]) detailedCustomer)[4]);
    }
}
