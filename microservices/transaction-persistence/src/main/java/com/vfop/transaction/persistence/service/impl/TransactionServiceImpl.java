package com.vfop.transaction.persistence.service.impl;

import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.persistence.model.Currency;
import com.vfop.transaction.persistence.model.Customer;
import com.vfop.transaction.persistence.model.Transaction;
import com.vfop.transaction.persistence.model.TransactionType;
import com.vfop.transaction.persistence.repository.CurrencyRepository;
import com.vfop.transaction.persistence.repository.CustomerRepository;
import com.vfop.transaction.persistence.repository.TransactionRepository;
import com.vfop.transaction.persistence.repository.TransactionTypeRepository;
import com.vfop.transaction.persistence.service.*;
import com.vfop.transaction.persistence.service.mapper.TransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;


/**
 * Created by flooprea on 10/8/2020
 */

@Service
public class TransactionServiceImpl implements TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

    private final TransactionRepository transactionRepository;

    private final TransactionMapper transactionMapper;

    private final TransactionTypeRepository transactionTypeRepository;

    private final CurrencyRepository currencyRepository;

    private final CustomerRepository customerRepository;

    @Autowired
    public TransactionServiceImpl(
            TransactionRepository transactionRepository,
            TransactionMapper transactionMapper,
            TransactionTypeRepository transactionTypeRepository,
            CurrencyRepository currencyRepository, CustomerRepository customerRepository) {
        this.transactionRepository = transactionRepository;
        this.transactionMapper = transactionMapper;
        this.transactionTypeRepository = transactionTypeRepository;
        this.currencyRepository = currencyRepository;
        this.customerRepository = customerRepository;
    }


    @Transactional
    @Override
    public Mono<TransactionDTO> createTransaction(TransactionDTO transactionDTO) {
        logger.info("Persisting transaction...");

        Transaction transaction = transactionMapper.transactionDTOToTransaction(transactionDTO);

        TransactionType transactionType = transactionTypeRepository.getOne(transactionDTO.getTransactionTypeId());
        Currency currency = currencyRepository.getOne(transactionDTO.getCurrencyId());
        transaction.setCurrency(currency);
        transaction.setTransactionType(transactionType);

        String payerCnp = transactionDTO.getPayer().getCnp();
        String payeeCnp = transactionDTO.getPayee().getCnp();

        Customer payer = customerRepository.findByCnp(payerCnp);
        Customer payee = customerRepository.findByCnp(payeeCnp);

        if (payer != null) {
            transaction.setPayer(payer);
        }

        if (payee != null) {
            transaction.setPayee(payee);
        }

        transaction = transactionRepository.save(transaction);

        return Mono.justOrEmpty(transactionMapper.transactionToTransactionDTO(transaction));
    }
}
