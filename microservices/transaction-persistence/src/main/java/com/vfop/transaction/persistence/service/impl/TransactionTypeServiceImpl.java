package com.vfop.transaction.persistence.service.impl;

import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import com.vfop.transaction.persistence.model.TransactionType;
import com.vfop.transaction.persistence.repository.TransactionTypeRepository;
import com.vfop.transaction.persistence.service.TransactionTypeService;
import com.vfop.transaction.persistence.service.mapper.TransactionTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * Created by flooprea on 10/8/2020
 */

@Service
public class TransactionTypeServiceImpl implements TransactionTypeService {

    private final TransactionTypeRepository transactionTypeRepository;

    private final TransactionTypeMapper transactionTypeMapper;

    @Autowired
    public TransactionTypeServiceImpl(TransactionTypeRepository transactionTypeRepository, TransactionTypeMapper transactionTypeMapper) {
        this.transactionTypeRepository = transactionTypeRepository;
        this.transactionTypeMapper = transactionTypeMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public Flux<TransactionTypeDTO> getAllTransactionTypes() {
        List<TransactionType> transactionTypes = transactionTypeRepository.findAll();

        return Flux.fromIterable(transactionTypes).map(transactionTypeMapper::transactionTypeToTransactionTypeDTO);
    }
}
