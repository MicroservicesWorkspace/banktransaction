package com.vfop.transaction.persistence.service.mapper;

import com.vfop.transaction.common.dto.core.account.AccountDTO;
import com.vfop.transaction.persistence.model.Account;
import org.mapstruct.Mapper;

/**
 * Created by flooprea on 10/8/2020
 */

@Mapper(componentModel = "spring", uses = {})
public interface AccountMapper {

    AccountDTO accountToAccountDTO(Account account);

    Account accountDTOToAccount(AccountDTO accountDTO);

    default Account accountFromId(Long id) {
        if (id == null) {
            return null;
        }

        Account account = new Account();
        account.setId(id);

        return account;
    }
}
