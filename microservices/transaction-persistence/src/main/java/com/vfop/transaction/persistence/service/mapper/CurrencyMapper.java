package com.vfop.transaction.persistence.service.mapper;

import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import com.vfop.transaction.persistence.model.Currency;
import org.mapstruct.Mapper;

/**
 * Created by flooprea on 10/8/2020
 */

@Mapper(componentModel = "spring", uses = {})
public interface CurrencyMapper {

    CurrencyDTO currencyToCurrencyDTO(Currency currency);

    Currency currencyDTOToCurrency(CurrencyDTO currencyDTO);

    default Currency currencyFromId(Long id) {
        if (id == null) {
            return null;
        }

        Currency currency = new Currency();
        currency.setId(id);

        return currency;
    }
}
