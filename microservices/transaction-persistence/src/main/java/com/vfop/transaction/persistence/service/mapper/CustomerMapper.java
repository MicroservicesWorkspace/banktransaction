package com.vfop.transaction.persistence.service.mapper;

import com.vfop.transaction.common.dto.core.customer.CustomerDTO;
import com.vfop.transaction.persistence.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Created by flooprea on 10/8/2020
 */

@Mapper(componentModel = "spring", uses = {AccountMapper.class})
public interface CustomerMapper {

    @Mapping(source = "account", target = "account")
    CustomerDTO customerToCustomerDTO(Customer customer);

    @Mapping(source = "account", target = "account")
    Customer customerDTOToCustomer(CustomerDTO customerDTO);

    default Customer customerFromId(Long id) {
        if (id == null) {
            return null;
        }

        Customer customer = new Customer();
        customer.setId(id);

        return customer;
    }
}
