package com.vfop.transaction.persistence.service.mapper;

import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.persistence.model.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Created by flooprea on 10/8/2020
 */

@Mapper(componentModel = "spring", uses = {CustomerMapper.class, TransactionTypeMapper.class, CurrencyMapper.class})
public interface TransactionMapper {

    @Mapping(source = "payer", target = "payer")
    @Mapping(source = "payee", target = "payee")
    @Mapping(source = "transactionType", target = "transactionType")
    @Mapping(source = "transactionType.id", target = "transactionTypeId")
    @Mapping(source = "currency", target = "currency")
    @Mapping(source = "currency.id", target = "currencyId")
    TransactionDTO transactionToTransactionDTO(Transaction transaction);

    @Mapping(source = "payer", target = "payer")
    @Mapping(source = "payee", target = "payee")
    @Mapping(source = "transactionTypeId", target = "transactionType")
    @Mapping(source = "currencyId", target = "currency")
    Transaction transactionDTOToTransaction(TransactionDTO transactionDTO);

    default Transaction transactionFromId(Long id) {
        if (id == null) {
            return null;
        }

        Transaction transaction = new Transaction();
        transaction.setId(id);

        return transaction;
    }
}
