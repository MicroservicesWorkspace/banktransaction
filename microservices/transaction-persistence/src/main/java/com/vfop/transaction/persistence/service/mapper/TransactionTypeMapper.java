package com.vfop.transaction.persistence.service.mapper;

import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import com.vfop.transaction.persistence.model.TransactionType;
import org.mapstruct.Mapper;

/**
 * Created by flooprea on 10/8/2020
 */

@Mapper(componentModel = "spring", uses = {})
public interface TransactionTypeMapper {

    TransactionTypeDTO transactionTypeToTransactionTypeDTO(TransactionType transactionType);

    TransactionType transactionTypeDTOToTransactionType(TransactionTypeDTO transactionTypeDTO);

    default TransactionType transactionTypeFromId(Long id) {
        if (id == null) {
            return null;
        }

        TransactionType transactionType = new TransactionType();
        transactionType.setId(id);

        return transactionType;
    }

}
