package com.vfop.transaction.validation.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

/**
 * Created by flooprea on 10/8/2020
 */

@Component
public interface MessageSources {

    String OUTPUT_TRANSACTIONS = "output-transactions";

    @Output(OUTPUT_TRANSACTIONS)
    MessageChannel outputTransactions();

}
