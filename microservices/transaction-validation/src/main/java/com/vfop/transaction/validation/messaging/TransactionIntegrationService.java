package com.vfop.transaction.validation.messaging;

import com.vfop.transaction.common.dto.Event;
import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by flooprea on 10/8/2020
 */

@EnableBinding(MessageSources.class)
@Component
public class TransactionIntegrationService {

    private final MessageSources messageSources;

    @Autowired
    public TransactionIntegrationService(MessageSources messageSources) {
        this.messageSources = messageSources;
    }

    public boolean sendToPersist(TransactionDTO transactionDTO) {
        messageSources.outputTransactions().send(MessageBuilder.withPayload(new Event<>(UUID.randomUUID(), transactionDTO)).build());

        return true;
    }
}
