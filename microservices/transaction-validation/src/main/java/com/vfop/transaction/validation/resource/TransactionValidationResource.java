package com.vfop.transaction.validation.resource;

import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.validation.exception.ExceptionDetails;
import com.vfop.transaction.validation.service.TransactionValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

/**
 * Created by flooprea on 10/8/2020
 */

@RestController
public class TransactionValidationResource {

    private static final Logger logger = LoggerFactory.getLogger(TransactionValidationResource.class);

    private final TransactionValidationService transactionValidationService;


    @Autowired
    public TransactionValidationResource(TransactionValidationService transactionValidationService) {
        this.transactionValidationService = transactionValidationService;
    }

    @PostMapping("/validate-transaction")
    public Mono<Boolean> validateTransaction(@RequestBody @Valid TransactionDTO transactionDTO) throws ExceptionDetails {
        logger.info("REST request to validate transactionDTO: {}...", transactionDTO);

        return transactionValidationService.validateTransaction(transactionDTO);
    }
}
