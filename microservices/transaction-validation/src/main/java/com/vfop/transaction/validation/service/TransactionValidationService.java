package com.vfop.transaction.validation.service;

import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.validation.exception.ExceptionDetails;
import com.vfop.transaction.validation.messaging.TransactionIntegrationService;
import org.iban4j.IbanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Date;

import static java.lang.Float.isNaN;

/**
 * Created by flooprea on 10/8/2020
 */

@Service
public class TransactionValidationService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionValidationService.class);

    private final TransactionIntegrationService transactionIntegrationService;

    @Autowired
    public TransactionValidationService(TransactionIntegrationService transactionIntegrationService) {
        this.transactionIntegrationService = transactionIntegrationService;
    }

    public Mono<Boolean> validateTransaction(TransactionDTO transactionDTO) throws ExceptionDetails {
        logger.info("Validating transaction {}...", transactionDTO.toString());

        String payerIban = transactionDTO.getPayer().getAccount().getIban();
        String payeeIban = transactionDTO.getPayee().getAccount().getIban();
        String payerCnp = transactionDTO.getPayer().getCnp();
        String payeeCnp = transactionDTO.getPayee().getCnp();


        IbanUtil.validate(payerIban);
        IbanUtil.validate(payeeIban);
        validateCnp(payerCnp);
        validateCnp(payeeCnp);

        if (payerCnp.equals(payeeCnp))
            throw new ExceptionDetails(new Date().getTime(), "Payer and Payee CNPs should be distinct");

        logger.info("Transaction validated!");

        return Mono.just(transactionIntegrationService.sendToPersist(transactionDTO));
    }


    private void validateCnp(String cnp) throws ExceptionDetails {
        int i, year, hashResult = 0;
        int[] cnpArr = new int[13];
        int[] hashTable = {2, 7, 9, 1, 4, 6, 3, 5, 8, 2, 7, 9};

        if (cnp.toCharArray().length != 13) {
            throw new ExceptionDetails(new Date().getTime(), "Validation failed! Invalid CNP: " + cnp + " length!");
        }

        for (i = 0; i < 13; i++) {
            cnpArr[i] = Integer.parseInt("" + cnp.charAt(i), 10);
            if (isNaN(cnpArr[i])) {
                throw new ExceptionDetails(new Date().getTime(), "Validation failed! Invalid CNP: " + cnp + " characters!");
            }
            if (i < 12) {
                hashResult = hashResult + (cnpArr[i] * hashTable[i]);
            }
        }
        hashResult = hashResult % 11;
        if (hashResult == 10) {
            hashResult = 1;
        }
        year = (cnpArr[1] * 10) + cnpArr[2];
        switch (cnpArr[0]) {
            case 1:
            case 2: {
                year += 1900;
            }
            break;
            case 3:
            case 4: {
                year += 1800;
            }
            break;
            case 5:
            case 6: {
                year += 2000;
            }
            break;
            case 7:
            case 8:
            case 9: {
                year += 2000;
                if (year > (Integer.parseInt("" + LocalDate.now().getYear(), 10) - 14)) {
                    year -= 100;
                }
            }
            break;
            default: {
                throw new ExceptionDetails(new Date().getTime(), "Validation failed! Invalid CNP: " + cnp + " start digit!");
            }
        }
        if (year < 1800 || year > 2099) {
            throw new ExceptionDetails(new Date().getTime(), "Validation failed! Invalid CNP: " + cnp);
        }

        if (cnpArr[12] != hashResult) {
            throw new ExceptionDetails(new Date().getTime(), "Validation failed! Invalid CNP: " + cnp + " digit control!");
        }
    }


}
