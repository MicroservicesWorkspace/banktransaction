package com.vfop.transaction.validation;

import com.vfop.transaction.common.dto.core.account.AccountDTO;
import com.vfop.transaction.common.dto.core.currency.CurrencyDTO;
import com.vfop.transaction.common.dto.core.customer.CustomerDTO;
import com.vfop.transaction.common.dto.core.transaction.TransactionDTO;
import com.vfop.transaction.common.dto.core.transactionType.TransactionTypeDTO;
import com.vfop.transaction.validation.exception.ExceptionDetails;
import com.vfop.transaction.validation.resource.TransactionValidationResource;
import com.vfop.transaction.validation.service.TransactionValidationService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = TransactionValidationResource.class)
class TransactionValidationApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private TransactionValidationService transactionValidationService;

    @Test
    public void givenTransaction_whenValidateTransaction_thenReturnTrue() throws ExceptionDetails {
        AccountDTO account1 = createAccountDTO("RO22INGB0000212337956621");
        AccountDTO account2 = createAccountDTO("RO32BCRB0000124367756356");

        CustomerDTO customer1 = createCustomerDTO("Ion", "Popescu", "1901110469825", account1);
        CustomerDTO customer2 = createCustomerDTO("Ionut", "Vasilache", "1920327386771", account2);

        CurrencyDTO currencyDTO = createCurrencyDTO(1L, "RON");
        TransactionTypeDTO transactionTypeDTO = createTransactionTypeDTO(1L, "IBAN_TO_IBAN");

        TransactionDTO transactionDTO = createTransactionDTO(customer1, customer2, currencyDTO, transactionTypeDTO, BigDecimal.valueOf(500));

        given(transactionValidationService.validateTransaction(transactionDTO)).willReturn(Mono.just(true));

        webTestClient
                .post()
                .uri("/validate-transaction")
                .body(BodyInserters.fromValue(transactionDTO))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Boolean.class)
                .isEqualTo(true);

    }

    @Test
    public void givenTransaction_whenValidateTransaction_thenThrowException() throws ExceptionDetails {
        AccountDTO account1 = createAccountDTO("RO22INGB0000212337956621");
        AccountDTO account2 = createAccountDTO("RO32BCRB0000124367756356");

        CustomerDTO customer1 = createCustomerDTO("Ion", "Popescu", "1901110469825", account1);
        CustomerDTO customer2 = createCustomerDTO("Ionut", "Vasilache", "1901110469825", account2);

        CurrencyDTO currencyDTO = createCurrencyDTO(1L, "RON");
        TransactionTypeDTO transactionTypeDTO = createTransactionTypeDTO(1L, "IBAN_TO_IBAN");

        TransactionDTO transactionDTO = createTransactionDTO(customer1, customer2, currencyDTO, transactionTypeDTO, BigDecimal.valueOf(500));

        given(transactionValidationService.validateTransaction(transactionDTO)).willThrow(ExceptionDetails.class);

        webTestClient
                .post()
                .uri("/validate-transaction")
                .body(BodyInserters.fromValue(transactionDTO))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .is4xxClientError();
    }

    private TransactionDTO createTransactionDTO(CustomerDTO payer, CustomerDTO payee,
                                                CurrencyDTO currencyDTO,
                                                TransactionTypeDTO transactionTypeDTO,
                                                BigDecimal amount) {
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setPayer(payer);
        transactionDTO.setPayee(payee);
        transactionDTO.setCurrencyId(currencyDTO.getId());
        transactionDTO.setTransactionTypeId(transactionTypeDTO.getId());
        transactionDTO.setAmount(amount);

        return transactionDTO;
    }

    private CustomerDTO createCustomerDTO(String firstName, String lastName, String cnp, AccountDTO accountDTO) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(firstName);
        customerDTO.setLastName(lastName);
        customerDTO.setCnp(cnp);
        customerDTO.setAccount(accountDTO);

        return customerDTO;
    }

    private AccountDTO createAccountDTO(String iban) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setIban(iban);

        return accountDTO;
    }

    private CurrencyDTO createCurrencyDTO(Long id, String name) {
        CurrencyDTO currencyDTO = new CurrencyDTO();
        currencyDTO.setId(id);
        currencyDTO.setName(name);

        return currencyDTO;
    }

    private TransactionTypeDTO createTransactionTypeDTO(Long id, String name) {
        TransactionTypeDTO transactionTypeDTO = new TransactionTypeDTO();
        transactionTypeDTO.setId(id);
        transactionTypeDTO.setName(name);

        return transactionTypeDTO;
    }

}
